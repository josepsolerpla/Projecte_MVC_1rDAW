<?php

$i18n=array(
  "Name" => "Nombre",
  "Phone" => "Telefono",
  "Type" => "Tipo",
  "Read" => "Mostrar",
  "Update" => "Actualizar",
  "Delete" => "Borrar",
  "User Information" => "Informacion del usuario",
  "Password" => "Contraseña",
  "Add User" => "Añadir Usuario",
  "Add Event" => "Añadir Evento",
  "Add Dummie" => "Añadir Dummie",
  "Create" => "Crear",
  "Delete all dummies" => "Borrar todos los dummies",
  "Number of dummies" => "Numero de dummies",
  "Search" => "Buscar Eventos",
  "Cart" => "Carrito",
  "Welcome" => "Bienvendio",
  "Salesoff" => "SE TERMINAN HOY!!!",
  "You need to create a user" => "Tienes que añadir un usuario",
  "Do you want delete the user " => "Quieres borrar el usuario",
  "Home Page" => "Inicio",
  "List Users" => "Lista de Usuarios",
  "List Events" => "Lista de Eventos",
  "About us" => "Sobre Nosotros",
  "Don't remember password ?" => "No te acuerdas de el usuario ?",
  /*ADD USER PAGE*/
  "Create user's" => "Crear usuario",
  "User's Name" => "Nombre de Usuario",
  "User's Surname" => "Apellido del Usuario",
  "User's email" => "Correo del Usuario",
  "User's number phone" => "Numero de Telefono",
  "Repeat PASSWORD" => "Repita la contraseña",
  "Birthday" => "Fecha de cumpleaños",
  "Done" => "Echo"
);

