CREATE DATABASE  IF NOT EXISTS `usuarios` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `usuarios`;
-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: usuarios
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `formulari`
--

DROP TABLE IF EXISTS `formulari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formulari` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `surname` text,
  `email` text,
  `phone` text,
  `password` text,
  `birthday` date DEFAULT NULL,
  `user_type` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formulari`
--

LOCK TABLES `formulari` WRITE;
/*!40000 ALTER TABLE `formulari` DISABLE KEYS */;
INSERT INTO `formulari` VALUES (1,'admin','admin','admin@gmail.com','+34123456789','admin','1998-07-28','admin'),(2,'user','user','user@gmail.com','+34123456788','user','1998-07-28','user'),(3,'owner','owner','owner@gmail.com','+34987654321','owner','1998-07-28','owner'),(16,'Yuse','Soler','josepsolerpla@gmail.com','+34963258741','josep','2018-02-27','user');
/*!40000 ALTER TABLE `formulari` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_like`
--

DROP TABLE IF EXISTS `user_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_like` (
  `id_user` varchar(45) NOT NULL,
  `id_event` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_like`
--

LOCK TABLES `user_like` WRITE;
/*!40000 ALTER TABLE `user_like` DISABLE KEYS */;
INSERT INTO `user_like` VALUES ('1','51'),('1','52'),('1','58'),('1','62'),('1','65'),('1','64'),('1','63'),('1','60'),('3','51'),('3','61'),('3','64');
/*!40000 ALTER TABLE `user_like` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-12 18:46:18
CREATE DATABASE  IF NOT EXISTS `events` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `events`;
-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: events
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `formulari`
--

DROP TABLE IF EXISTS `formulari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formulari` (
  `id_event` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text,
  `propietario` text,
  `telf_contact` text,
  `dirigido_a` text,
  `presupuesto` text,
  `hora_inicio` time DEFAULT NULL,
  `hora_fin` time DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `lng` text,
  `lat` text,
  `ticket_price` int(11) DEFAULT '0',
  PRIMARY KEY (`id_event`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formulari`
--

LOCK TABLES `formulari` WRITE;
/*!40000 ALTER TABLE `formulari` DISABLE KEYS */;
INSERT INTO `formulari` VALUES (51,'Diania','Ontinyent','+34112233665','+16','10000','20:50:00','05:23:00','2018-02-14','-0.544853','38.815319',50),(52,'Festivern','Tabernes','+34123456263','+16','60000','00:00:00','00:00:00','2018-02-16','-0.544858','38.815333',50),(58,'dummie','dummie','+34966825324','+16','2 ','00:00:00','00:00:00','1998-07-28','10','10',15),(60,'--dummie--2','dummie','dummie','+18','0â‚¬','00:00:00','00:00:00','1998-07-28','10','10',0),(61,'--dummie--3','dummie','dummie','+18','0â‚¬','00:00:00','00:00:00','1998-07-28','10','10',0),(62,'--dummie--4','dummie','dummie','+18','0â‚¬','00:00:00','00:00:00','1998-07-28','10','10',0),(63,'--dummie--5','dummie','dummie','+18','0â‚¬','00:00:00','00:00:00','1998-07-28','10','10',0),(64,'--dummie--6','dummie','dummie','+18','0â‚¬','00:00:00','00:00:00','1998-07-28','10','10',0),(65,'--dummie--7','dummie','dummie','+18','0â‚¬','00:00:00','00:00:00','1998-07-28','10','10',0),(67,'--dummie--0','dummie','dummie','+18','0â‚¬','00:00:00','00:00:00','1998-07-28','10','10',0),(68,'--dummie--1','dummie','dummie','+18','0â‚¬','00:00:00','00:00:00','1998-07-28','10','10',0),(69,'--dummie--2','dummie','dummie','+18','0â‚¬','00:00:00','00:00:00','1998-07-28','10','10',0),(70,'--dummie--3','dummie','dummie','+18','0â‚¬','00:00:00','00:00:00','1998-07-28','10','10',0);
/*!40000 ALTER TABLE `formulari` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tickets` (
  `id_event` int(11) DEFAULT NULL,
  `code_ticket` varchar(255) NOT NULL,
  `id_client` int(11) DEFAULT NULL,
  PRIMARY KEY (`code_ticket`),
  KEY `id_event` (`id_event`),
  KEY `id_client` (`id_client`),
  CONSTRAINT `tickets_ibfk_1` FOREIGN KEY (`id_event`) REFERENCES `formulari` (`id_event`),
  CONSTRAINT `tickets_ibfk_2` FOREIGN KEY (`id_client`) REFERENCES `usuarios`.`formulari` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tickets`
--

LOCK TABLES `tickets` WRITE;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
INSERT INTO `tickets` VALUES (51,'event-51-1',2),(51,'event-51-10',2),(51,'event-51-11',2),(51,'event-51-12',NULL),(51,'event-51-13',NULL),(51,'event-51-14',NULL),(51,'event-51-15',NULL),(51,'event-51-2',NULL),(51,'event-51-3',NULL),(51,'event-51-4',NULL),(51,'event-51-5',NULL),(51,'event-51-6',NULL),(51,'event-51-7',NULL),(51,'event-51-8',NULL),(51,'event-51-9',NULL),(52,'event-52-1',2),(52,'event-52-10',NULL),(52,'event-52-2',NULL),(52,'event-52-3',NULL),(52,'event-52-4',NULL),(52,'event-52-5',NULL),(52,'event-52-6',NULL),(52,'event-52-7',NULL),(52,'event-52-8',NULL),(52,'event-52-9',NULL),(58,'event-58-1',1),(58,'event-58-2',2),(58,'event-58-3',2),(58,'event-58-4',NULL),(58,'event-58-5',NULL),(60,'event-60-1',NULL),(60,'event-60-2',NULL),(60,'event-60-3',NULL),(60,'event-60-4',NULL),(60,'event-60-5',NULL),(60,'event-60-6',NULL),(64,'event-64-1',NULL),(64,'event-64-10',NULL),(64,'event-64-2',NULL),(64,'event-64-3',NULL),(64,'event-64-4',NULL),(64,'event-64-5',NULL),(64,'event-64-6',NULL),(64,'event-64-7',NULL),(64,'event-64-8',NULL),(64,'event-64-9',NULL);
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-12 18:46:18
