<?php

$i18n=array(
  /*LIST*/
  "Name" => "Name",
  "Phone" => "Phone",
  "Type" => "Type",
  "Read" => "Read",
  "Update" => "Update",
  "Delete" => "Delete",
  "User Information" => "User Information",
  "Password" => "Password",
  "Add User" => "Add User",
  "Add Event" => "Add Event",
  "Add Dummie" => "Add Dummie",
  "Create" => "Create",
  "Number of dummies" => "Number of dummies",
  "Delete all dummies" => "Delete all dummies",
  "You need to create a user" => "You need to create a user",
  "Search" => "Search Event",
  "Cart" => "Cart",
  "Welcome" => "Welcome",
  "Salesoff" => "SALES OFF TODAY!!!",
  /*DELETE*/
  "Do you want delete the user" => "Do you want delete the user",
  /*MENU*/
  "Home Page" => "Home Page",
  "List Users" => "List of Users",
  "List Events" => "List of Events",
  "About us" => "About us",
  "Don't remember password ?" => "Don't remember password ?",
  /*ADD USER PAGE*/
  "Create user's" => "Create user's",
  "User's Name" => "User's Name",
  "User's Surname" => "User's Surname",
  "User's email" => "User's email",
  "User's number phone" => "User's phone",
  "Repeat PASSWORD" => "Repeat PASSWORD",
  "Birthday" => "Birthday",
  "Done" => "Done"

);
