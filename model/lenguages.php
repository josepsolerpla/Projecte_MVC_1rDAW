<?php
	if( !isset($_SESSION["lang"]) ){
	    $_SESSION['lang'] = 'En';
	}

	if(isset($_POST['lang'])){
		$_SESSION['lang'] = $_POST['lang'];
	}

	switch($_SESSION['lang'])
	{
	  case 'Es':
	    include('model/es.php');
	  break;

	  default:
	  case 'En':
	    include('model/en.php');
	  break;
	}