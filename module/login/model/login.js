function tryToLogIn(){
 	var on = "true";
		var username = document.getElementById("username").value;
		var nameRegex = /^[a-zA-Z\-]+$/;
		if(!nameRegex.test(username)){
			document.getElementById("error_username").style.backgroundColor ='rgb( 146 , 0 ,0 , .5)';
        	document.getElementById('error_username').innerHTML = "Insert a username";
        	on="false";
		}else {
		document.getElementById('error_username').innerHTML = "";
		}
		var password = document.getElementById("password").value;
		if(password == ""){
			document.getElementById("error_password").style.backgroundColor ='rgb( 146 , 0 ,0 , .5)';
        	document.getElementById('error_password').innerHTML = "Insert a password";
			on="false";
		}else{			
        document.getElementById('error_password').innerHTML = "";
		}
		if(on == "true"){
			$.get("module/login/controller/controller_login.php?op=login&username="+username+"&password="+password, function (data, status) {
	            console.log(data);
	            var json = JSON.parse(data);
	            if(json === 'error') {
	                window.location.href='index.php?page=503';
	            }else{
	            	if(json == 'true'){
	            		window.location.href="index.php?page=controller_home&op=list";
        				document.getElementById('error_username').innerHTML = "";
	            	}
	            	if(json == 'false'){
	            		document.getElementById("error_username").style.backgroundColor ='rgb( 146 , 0 ,0 , .5)';
	            		document.getElementById("error_username").style.width = '250px';
        				document.getElementById('error_username').innerHTML = "Username or password incorrect";
	            	}
	            };
	        });
	    }
}
$(document).ready(function () {
	//Login display
	var active = "false";
	$('#login-trigger').click(function(){
	    $(this).next('#login-content').slideToggle();
	    $(this).toggleClass('active');          
	    if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
	      else $(this).find('span').html('&#x25BC;')
    })
	$('#content').click(function (e) {
	    if($("#login-trigger").hasClass('active')){
	    	$("#login-trigger").toggleClass('active');
	    	if ($("#login-trigger").hasClass('active')) $("#login-trigger").find('span').html('&#x25B2;')
	     	 else $("#login-trigger").find('span').html('&#x25BC;')
	     	$("#login-trigger").next('#login-content').slideToggle();
    	}
	});
	//Butons del login
	$('#login_log').click(function () {
		tryToLogIn();
    });
	$('#password').bind('keypress', function(e) {
	    if(e.keyCode==13){
			tryToLogIn();
	    }
	});
    
    $('#login_off').click(function () {
    	$.get("module/login/controller/controller_login.php?op=logg_off", function (data, status) {
	            window.location.href="index.php?page=controller_home&op=list";
	    });
    });
    $('#remember').click(function () {
        alert();
    });
    
});