<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . '/Projecte_MVC_1rDAW/';
    include($path."module/login/model/DAOLogin.php");
    if (!isset($_SESSION)) {
	  session_start();
	}
	if (!isset($_SESSION['login_usertype'])) {
		$_SESSION['login_usertype']="";
	}
	if(!isset($_SESSION['username'])){
		$_SESSION['username']="";
	}
	switch ($_GET['op']) {
		case 'login':
			try{
				$daologin = new DAOLogin();
				$rdo = $daologin->login_usernamepass();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
    			echo json_encode("false");
                exit;
    		}else{
    			$_SESSION['login_usertype']=$rdo->user_type;
    			$_SESSION['username']=$rdo->name;
    			$_SESSION['id']=$rdo->id;
    			echo json_encode("true");
                exit;
    		}
			break;
		case 'logg_off':
			$_SESSION['login_usertype']="";
    		$_SESSION['username']="";
    		$_SESSION['id']="";
			echo json_encode("true");
			break;
		case 'remember':
			break;
		default:
            include("view/includes/error404.php");
			break;
		}