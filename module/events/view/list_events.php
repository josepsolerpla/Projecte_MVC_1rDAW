<div id="list">
	<div id="listtittle">
			<?php echo '<h1>' .$i18n["List Events"] .'</h1>';?>
	</div>
	<div id="menu_listevents">
		<p>
			<?php 
				echo '<a style="position: absolute;left: 500px;" class="addlist" href="index.php?page=controller_events&op=create">' .$i18n["Add Event"] .'</a>';
				echo '<a style="position: absolute;left: 1100px;" href="index.php?page=controller_dummies&op=create">' .$i18n["Add Dummie"] .'</a>';
			?>
            <?php echo '<input id="search_name" type="text" placeholder="' .$i18n["Search"] .'">';?>
		</p>
	</div>
	<div id="tablelist_events"></div>
</div>
<section id="user_modal">
    <div id="details_user" hidden>
        <div id="details">
            <div id="container">
                Event name: <div id="nombre"></div></br>
                Owner : <div id="propietario"></div></br>
                Phone : <div id="telf_contact"></div></br>
                Target: <div id="dirigido_a"></div></br>
                Value : <div id="presupuesto"></div></br>
                Start time: <div id="hora_inicio"></div></br>
                End time: <div id="hora_fin"></div></br>
                Date: <div id="fecha"></div></br>
            </div>
        </div>
    </div>
</section>