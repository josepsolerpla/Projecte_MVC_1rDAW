<div id="update_users">
		<div>
			<h1>Modify the user</h1><input id="id_event" value="<?php echo $_GET['id']?>" readonly="readonly"></input>
		</div>
		<div id="slot">
          <label for="name">Event's name :</label>
          <input name="name" class="name" type="text" id="name" placeholder="name" value="" >
          <span id="e_name" class='error_js'></span>
        </div> 
        <div id="slot">
            <label for="owner">Owner's name :</label>
            <input name="owner" class="owner" type="text" id="owner" placeholder="owner" value="" >
            <span id="e_owner" class='error_js'></span>
        </div>
        <div id="slot">
            <label for="phone">Number phone :</label>
            <input name="phone" class="phone" type="text" id="phone" placeholder="number phone" value="">
            <span id="e_phone" class='error_js'></span>
        </div>
        <div id="slot_forbuttons" style="margin-left:3em">
          <label for="phone">Target :</label>
          <input type="radio" name="recommended" id="recommended+12" value="+12" >+12</input>
          <input type="radio" name="recommended" id="recommended+16" value="+16" >+16</input>
          <input type="radio" name="recommended" id="recommended+18" value="+18" >+18</input>
        </div>
        <div id="slot">
            <label for="ticket_value">Value :</label>
            <input name="ticket_value" class="ticket_value" type="text" id="ticket_value" placeholder="ticket_value" value="" >
            <span id="e_ticket_value" class='error_js'></span>
        </div>
        <div id="slot">
            <label for="value">Value :</label>
            <input name="value" class="value" type="text" id="value" placeholder="value" value="" >
            <span id="e_value" class='error_js'></span>
        </div>
        <div id="slot">
            <label for="s_time">Start time :</label>
            <input name="s_time" class="s_time" type="text" id="s_time" placeholder="s_time" value="" >
        </div>
        <div id="slot">
            <label for="f_time">Finish time :</label>
            <input name="f_time" class="f_time" type="text" id="f_time" placeholder="f_time" value="" >
        </div>
        <div id="slot">
            <label for="date">Event's date :</label>
            <input name="date" class="date" type="text" id="datepicker" placeholder="date" value="" readonly="readonly">
            <span id="e_date" class='error_js'></span>
        </div>
        <div id="slot">
            <h4>Location :</h4>
            <label for="lng">Longitud :</label>
            <input name="lng" class="lng" type="text" id="lng" placeholder="lng" value="" >
            <label for="lat">Latitud :</label>
            <input name="lat" class="lat" type="text" id="lat" placeholder="lat" value="" >
        </div>
    <input id="update" type="submit" value="Update"/>
</div>