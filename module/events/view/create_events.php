	<div id="create_users">
    <h1>Create Event's</h1> 
    <form name="formulario_espetaculo" id="formulario_espetaculo" method="post">
      <div id="formulari">
        <div id="slot">
	       <p>
	        <label for="name">Event's name :</label>
	        <input name="name" class="name" type="text" id="name" placeholder="name" value="<?php @$_POST['name'] ?>" >
	        <span id="e_name" class='error_js'></span>
	       </p>
        </div> 
        <div id="slot">
        	<p>
          	<label for="owner">Owner's name :</label>
        		<input name="owner" class="owner" type="text" id="owner" placeholder="owner" value="<?php @$_POST['owner'] ?>" >
          	<span id="e_owner" class='error_js'></span>
          </p>
        </div>
        
        <div id="slot">
          <p>
            <label for="phone">Number phone :</label>
            <input name="phone" class="phone" type="text" id="phone" placeholder="number phone" value="<?php @$_POST['phone'] ?>">
            <span id="e_phone" class='error_js'></span>
          </p>
        </div>
        <div id="slot_forbuttons">
          <label for="phone">Target :</label>
          <input type="radio" name="recommended" id="recommended" value="+12" /> +12
          <input type="radio" name="recommended" id="recommended" value="+16" /> +16
          <input type="radio" name="recommended" id="recommended" value="+18" checked="checked" /> +18
        </div>
        <div id="slot">
          <p>
            <label for="value">Value :</label>
            <input name="value" class="value" type="text" id="value" placeholder="value" value="<?php @$_POST['value'] ?>" >
            <span id="e_value" class='error_js'></span>
          </p>
        </div>
        <div id="slot">
          <p>
            <label for="ticket_price">Value of 1 ticket :</label>
            <input name="ticket_price" class="ticket_price" type="text" id="ticket_price" placeholder="ticket_price" value="<?php @$_POST['ticket_price'] ?>" >
            <span id="e_ticket_price" class='error_js'></span>
          </p>
        </div>
        <div id="slot">
          <p>
            <label for="s_time">Start time :</label>
            <input name="s_time" class="s_time" type="text" id="s_time" placeholder="s_time" value="<?php @$_POST['s_time'] ?>" >
          </p>
        </div>

        <div id="slot">
          <p>
            <label for="f_time">Finish time :</label>
            <input name="f_time" class="f_time" type="text" id="f_time" placeholder="f_time" value="<?php @$_POST['f_time'] ?>" >
          </p>
        </div>

        <div id="slot">
        	<p>
        	  <label for="date">Event's date :</label>
        	  <input name="date" class="date" type="text" id="datepicker" placeholder="date" value="<?php @$_POST['date'] ?>" readonly="readonly">
        	  <span id="e_date" class='error_js'></span>
          </p>
        </div>
        <div id="slot">
          <p>
            <h4>Location :</h4>
          </p>
          <p>
            <label for="lng">Longitud :</label>
            <input name="lng" class="lng" type="text" id="lng" placeholder="lng" value="<?php @$_POST['lng'] ?>" >
          </p>
          <p>
            <label for="lat">Latitud :</label>
            <input name="lat" class="lat" type="text" id="lat" placeholder="lat" value="<?php @$_POST['lat'] ?>" >
          </p>
        </div>
        <div id="button">
          <input name="done" id="createbutt" class="done" type="button" value="Done"/>
        </div>
      </div>
    </form>
    </div>