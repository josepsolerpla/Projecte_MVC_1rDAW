<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . '/Projecte_MVC_1rDAW/';
//	CHECK IF THE SESSION IS ALLOWED TO ENTER
	if (!isset($_SESSION)) {
	  session_start();
	}
//	SET THE PATH FOR EXTERNAL INCLUDES
	include($path."module/events/model/DAOEvents.php");
	include($path."module/events/model/DAOTickets.php");
	include($path."module/events/model/find_events.php");
// 	CONTROLLER WITH SWITCH
    if (!isset($_GET["op"])) {
            $_GET["op"]="list";
        }    
	switch ($_GET['op']) {
        case 'val_exist_event':
            try{
                $daoevent = new FINDuser();
                $rdo = $daoevent->find_eventbyname($_POST['name']);

            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if($rdo->num_rows >= 1){
                echo json_encode("0");
                exit;
            }else{
                echo json_encode("1");
                exit;
            }
            break;
		case 'list':
			if(isset($_GET['name'])){
				$_SESSION['name'] = $_GET['name'];			
			}
			try{
				$daoevent = new DAOEvent();
				$rdo_action = $daoevent->select_event_name();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }	
            if(!$rdo_action){
    			echo json_encode("error");
                exit;
    		}else{
                echo json_encode($rdo_action);
                exit;
    		}
			break;
		case 'create':
            try{
                $_SESSION['event']=$_POST;
                $event = $_SESSION['event'];
                $daoevent = new DAOEvent();
                $rdo = $daoevent->new_event($event);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("false");
                exit;
            }else{
                echo json_encode("true");
                exit;
            }
		 	include 'module/events/view/create_events.php';
		 	break;
		 case 'delete':
		 	try{
                $daoevent = new DAOEvent();
            	$rdo = $daoevent->delete_event($_POST['id']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
    			echo json_encode("false");
                exit;
    		}else{
                echo json_encode("true");
                exit;
    		}
		 	break;
		 case 'update':
            try{
                $daoevent = new DAOEvent();
            	$rdo = $daoevent->update_event($_POST);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
    			echo json_encode("false");
                exit;
    		}else{
                echo json_encode("true");
                exit;
    		}
            break;
        case 'update_info':
        	try{
        		if(isset($_GET['update_id'])){
					$_SESSION['update_id'] = $_GET['update_id'];			
				}
                $daoevent = new FINDuser();
            	$rdo = $daoevent->find_eventbyid();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
    			echo json_encode("error");
                exit;
    		}else{
                echo json_encode($rdo);
                exit;
    		}
            break;
        case 'read_modal':
        	try{
                $daoevent = new DAOEvent();
            	$rdo = $daoevent->select_events_model($_GET['modal']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
    			echo json_encode("error");
                exit;
    		}else{
    		    $user=get_object_vars($rdo);
                echo json_encode($user);
                exit;
    		}
            break;
        case 'create_tickets':
          try{
              	$daotickets = new DAOTickets();
            	$cant = [];
            	$cant['cant']=$_POST['cant'];
            	$cant['id_event']=$_POST['id_event'];
     			$tickets = $daotickets->create_tickets($cant);
                echo json_encode($tickets);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
			if(!$tickets){
                echo json_encode("0");
                exit;
            }else{
                echo json_encode("1");
                exit;
            }
        	break;
        case 'list_tickets':
          try{
                $daotickets = new DAOTickets();
                $cant = [];
                $cant['id_event']=$_GET['id_event'];
                $tickets = $daotickets->list_tickets($cant);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$tickets){
                echo json_encode("error");
                exit;
            }else{
                echo json_encode($tickets);
                exit;
            }
            break;
		default:
            include("view/includes/error404.php");
			break;
		}