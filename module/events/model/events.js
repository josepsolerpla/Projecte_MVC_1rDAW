var find_byname;
$(document).ready(function() {
//AUTOCOMPLETE
    $.get("module/events/controller/controller_events.php?op=list&name=", function (data) {
        find_byname = JSON.parse(data);
        if(find_byname === 'error') {
                window.location.href='index.php?page=503';
            }else{
                loadName();    
            };
    });
    if(document.getElementById("tablelist_events") !== null){
        $.get("module/events/controller/controller_events.php?op=list&name=", function (data, status) {
            var json = JSON.parse(data);
                json.forEach(function(rdo){
                    var newElement = document.createElement('div');
                    newElement.id = "eventdivlist_02";
                    newElement.className = rdo.id_event;
                    newElement.innerHTML = '<h3>'+rdo.nombre+'</h3>\<p>'+rdo.fecha+'</p>';
                    newElement.innerHTML = newElement.innerHTML + '<div class="event_read" id="'+rdo.id_event+'">Info</div>';
                    newElement.innerHTML = newElement.innerHTML + '<div class="event_update" id="'+rdo.id_event+'">Update</div>';
                    newElement.innerHTML = newElement.innerHTML + '<div class="event_delete" id="'+rdo.id_event+'">Delete</div>';
                    newElement.innerHTML = newElement.innerHTML + '<div class="event_tickets" id="'+rdo.id_event+'">Tickets</div>';
                    document.getElementById("tablelist_events").appendChild(newElement);
                });
        });
        $('#search_name').on('input', function() {
            var search_name = document.getElementById("search_name").value;
            load_info(search_name);
        });
//UPDATE CLICK BUTTON
        $(document).on('click', ".event_update", function() {
            window.location.href="index.php?page=controller_events&op=update&id="+$(this).context.id;
        });
    }
//DELETE ALERT
    $(document).on('click', ".event_delete", function() {
      var idName = $(this).attr('id');
      if (confirm('Are you sure ?')) {
          $.ajax({
                url: "module/events/controller/controller_events.php?op=delete",
                type: 'POST',
                data : {
                    "id": idName,   
                },
                success: function(data) {
                    console.log(data);
                    alert("Succesfull deleted");
                },
          })
      }
      window.location.href="index.php?page=controller_events&op=list";
    });
//TICKETS
    $(document).on('click', ".event_tickets", function() {
        console.log($(this).attr('id'));
        window.location.href="index.php?page=controller_events&op=tickets&id="+$(this).context.id;
    });
//READ MODAL
    $(document).on('click', ".event_read", function() {
        var id = this.getAttribute('id');
        $.get("module/events/controller/controller_events.php?op=read_modal&modal=" + id, function (data, status) {
            var json = JSON.parse(data);
            if(json === 'error') {
                window.location.href='index.php?page=503';
            }else{
                $("#nombre").html(json.nombre);
                $("#propietario").html(json.propietario);
                $("#telf_contact").html(json.telf_contact);
                $("#dirigido_a").html(json.dirigido_a);
                $("#presupuesto").html(json.presupuesto+"€");
                $("#hora_inicio").html(json.hora_inicio);
                $("#hora_fin").html(json.hora_fin);
                $("#fecha").html(json.fecha);
     
                $("#details_user").show();
                $("#user_modal").dialog({
                    width: 850, height: 500,resizable: "false",modal: "true",
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    },
                    show: {
                        //effect: "blind",
                        duration: 0},
                    hide: {
                        //effect: "explode",
                        duration: 0}
                })
            }
        });
    });
});