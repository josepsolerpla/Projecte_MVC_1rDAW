$(document).ready(function() {
//UPDATE
    if(document.getElementById("update_users") !== null){
        var id_event = document.getElementById('id_event').value;
        var original_name;
        //FILL DATA
        $.get("module/events/controller/controller_events.php?op=update_info&update_id="+id_event, function (data, status) {
            var json = JSON.parse(data);
            var asdasd = "200€";
            original_name = json[0].nombre;
            $("#name").val(json[0].nombre);
            $("#owner").val(json[0].propietario);
            $("#phone").val(json[0].telf_contact);
            $("#value").val(json[0].presupuesto+"€");
            $("#s_time").val(json[0].hora_inicio);
            $("#f_time").val(json[0].hora_fin);
            $("#datepicker").val(json[0].fecha);
            $("#ticket_value").val(json[0].ticket_price+"€");
            $("#lng").val(json[0].lng);
            $("#lat").val(json[0].lat);
            var dirig = "#recommended\\"+json[0].dirigido_a;
            $(dirig).attr( "checked", true );
        });
        //VALIDATE IF EXIST
        $(document).on('keyup', "#name", function() {
            var update_name = document.getElementById('name').value;
            console.log(update_name);
            if(update_name !== original_name){
                $.ajax({
                    url: "module/events/controller/controller_events.php?op=val_exist_event",
                    type: 'POST',
                    dataType: 'json',
                    data : {
                        "name": update_name,
                    },
                    success: function(data) {
                        console.log(data);
                        if(data == 0){
                            document.getElementById('name').style.color ='rgb( 146 , 0 ,0 , .5)';
                            document.getElementById('e_name').innerHTML = "The name is already used";
                        }else{
                            document.getElementById('name').style.color ='';
                            document.getElementById('e_name').innerHTML = "";
                        }
                    },
              })
            }
        });

        //BUTTON FOR UPDATE
        $(document).on('click', "#update", function() {
            console.log(document.getElementById('ticket_value').value.replace(/[^a-zA-Z 0-9.]+/g,' '));
            //VALIDATE REST
            var result = true;
            var name = document.getElementById('name').value;
            var owner = document.getElementById('owner').value;
            var phone = document.getElementById('phone').value;
            var value = document.getElementById('value').value.replace(/[^a-zA-Z 0-9.]+/g,'');
            var tick_val = document.getElementById('ticket_value').value.replace(/[^a-zA-Z 0-9.]+/g,'')
            console.log(value+tick_val);
            var v_tick = validate_value(tick_val);
            var v_name = validate_name(name);
            var v_owner = validate_owner(owner);
            var v_phone = validate_phone(phone);
            var v_value = validate_value(value);
            if (!v_tick) {
                document.getElementById('e_ticket_value').innerHTML = "The value is invalid";
                result = false;
            } else {
                document.getElementById('e_ticket_value').innerHTML = "";
            }
            if (!v_name) {
                document.getElementById('e_name').innerHTML = "The name is invalid";
                result = false;
            } else {
                document.getElementById('e_name').innerHTML = "";
            }
            if (!v_owner) {
                document.getElementById('e_owner').innerHTML = "The owner name is invalid";
                result = false;
            } else {
                document.getElementById('e_owner').innerHTML = "";
            }
            if (!v_phone) {
                document.getElementById('e_phone').innerHTML = "The phone its incorrect";
                result = false;
            } else {
                document.getElementById('e_phone').innerHTML = "";
            }
            if (!v_value) {
                document.getElementById('e_value').innerHTML = "The value number its incorrect";
                result = false;
            } else {
                document.getElementById('e_value').innerHTML = "";
            }
            if(result != false){
                $.ajax({
                    url: "module/events/controller/controller_events.php?op=update",
                    type: 'POST',
                    data : {
                        "id" : document.getElementById('id_event').value,
                        "name": document.getElementById('name').value,
                        "owner": document.getElementById('owner').value,
                        "phone": document.getElementById('phone').value,
                        "value": document.getElementById('value').value.replace(/[^a-zA-Z 0-9.]+/g,' '),
                        "s_time": document.getElementById('s_time').value,
                        "f_time": document.getElementById('f_time').value,
                        "date": document.getElementById('datepicker').value,
                        "lng": document.getElementById('lng').value,
                        "lat": document.getElementById('lat').value,
                        "ticket_price": document.getElementById('ticket_value').value.replace(/[^a-zA-Z 0-9.]+/g,' '),
                        "recommended": "+16",
                    },
                    success: function(data) {
                        alert("Succesfull updated");
                        window.location.href="index.php?page=controller_events&op=list";
                    },
              })
            }else{
                document.getElementById('update').style.color ='rgb( 146 , 0 ,0 , .5)';
            }
        });
    }
});