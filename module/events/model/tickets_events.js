$(document).ready(function() {
 if(document.getElementById("divtickets") !== null){
        load_ticket(GetURLParameter('id'));
        $("#cant_tickets").on('keyup', function(){
            if (isNaN($(this).val()) || $(this).val()===null || parseInt($(this).val()) < 0) {
                $(this).css("color","red");
                $("#submit_tickets").css("color","red");
                $("#submit_tickets")[0].disabled = true;
            }else {
                $(this).css("color","black");
                $("#submit_tickets").css("color","black");
                $("#submit_tickets")[0].disabled = false;
            }
        });
        $(document).on('click', '#submit_tickets', function() {
            var id_event_ticket = GetURLParameter('id');
            var cant_tickets = document.getElementById("cant_tickets").value;
            $.ajax({
                url: "module/events/controller/controller_events.php?op=create_tickets",
                type: 'POST',
                data : {
                    "cant": cant_tickets,
                    "id_event": id_event_ticket, 
                },
                success: function(data) {
                    console.log(data);
                      load_ticket(GetURLParameter('id'));
                },
          })
        });
        $(document).on('click', '#update_info', function() {
            var id_event_ticket = GetURLParameter('id');
            load_ticket(id_event_ticket);
        });
    }
});