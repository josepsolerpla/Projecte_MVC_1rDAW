<?php	$path = $_SERVER['DOCUMENT_ROOT'] . '/Projecte_MVC_1rDAW/';
    include($path."model/connect.php");
	class DAOEvent{
		function new_event($event){
			 $sql = "INSERT INTO formulari(nombre, propietario, telf_contact, dirigido_a, presupuesto,hora_inicio,hora_fin,fecha,lng,lat,ticket_price) 
			 VALUES ('$event[name]', '$event[owner]', '$event[phone]','$event[recommended]','$event[value]','$event[s_time]',
			 	'$event[f_time]', '$event[date]', '$event[lng]', '$event[lat]' , '$event[ticket_price]')";
            $conexion = connection_event::con();
            $res = mysqli_query($conexion, $sql);
            connection_event::close($conexion);
			return $res;
		}
		
		function select_all_events(){
			$sql = "SELECT * FROM formulari ORDER BY id_event ASC";
			$conexion = connection_event::con();
			$res = mysqli_query($conexion, $sql);
			connection_event::close($conexion);
			return $res;	
		}

		function select_events($id){
			$sql = "SELECT * FROM formulari WHERE id_event='$id'";

			$conexion = connection_event::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
			connection_event::close($conexion);
			return $res;
		}

		function update_event($all){
			$sql = "UPDATE formulari SET nombre='$all[name]', propietario='$all[owner]', telf_contact='$all[phone]', 
			dirigido_a='$all[recommended]', presupuesto='$all[value]', hora_inicio='$all[s_time]',hora_fin='$all[f_time]',
			 fecha='$all[date]', lng = '$all[lng]', lat = '$all[lat]' , ticket_price = '$all[ticket_price]' WHERE id_event='$all[id]'";
            $conexion = connection_event::con();
            $res = mysqli_query($conexion, $sql);
            connection_event::close($conexion);
			return $res;
		}

		function delete_event($id){
			$sql = "DELETE FROM formulari WHERE id_event='$id'";
            $sql_1 = "DELETE FROM tickets WHERE id_event='$id'";

			$conexion = connection_event::con();
			$conexion_1 = connection_tickets::con();
            $res = mysqli_query($conexion, $sql);
            $res2 = mysqli_query($conexion_1, $sql_1);
            connection_event::close($conexion);
            connection_tickets::close($conexion_1);

			return $res;
		}
		function delete_event_dummies(){
			$sql = "DELETE FROM formulari WHERE nombre LIKE '--dummie--%'";

			$conexion = connection_event::con();
            $res = mysqli_query($conexion, $sql);
            connection_event::close($conexion);
			return $res;
		}
		function select_events_model($id){
			$sql = 'SELECT * FROM formulari WHERE id_event='.$id;

			$conexion = connection_event::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
			connection_event::close($conexion);
			return $res;
		}
		function select_event_name(){
				$sql = 'SELECT * FROM formulari WHERE nombre LIKE "'.$_SESSION['name'].'%" ';
				$conexion = connection_event::con();
				$res = mysqli_query($conexion, $sql);
				foreach ($res as $row) {
					$form[] = array_merge($row);
				}
				connection_event::close($conexion);
				return $form;
		}
	}