$(function() {
		$('#demo1').datepicker({
			dateFormat: 'yy/mm/dd', 
			changeMonth: true, 
			changeYear: true, 
			yearRange: '1950:2017',
			minDate: 1000,
		});
});