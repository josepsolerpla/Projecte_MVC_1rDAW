$(document).ready(function() {
	$(document).on('keyup', "#name", function() {
            var update_name = document.getElementById('name').value;
                $.ajax({
                    url: "module/events/controller/controller_events.php?op=val_exist_event",
                    type: 'POST',
                    dataType: 'json',
                    data : {
                        "name": update_name,
                    },
                    success: function(data) {
                        if(data == 0){
                            document.getElementById('name').style.color ='rgb( 146 , 0 ,0 , .5)';
                            document.getElementById('e_name').innerHTML = "The name is already used";
                        }else{
                            document.getElementById('name').style.color ='';
                            document.getElementById('e_name').innerHTML = "";
                        }
                    },
              })
        });

	$(document).on('click', "#createbutt", function() {
        //VALIDATE REST
        var result = true;
        var name = document.getElementById('name').value;
        var owner = document.getElementById('owner').value;
        var phone = document.getElementById('phone').value;
        var value = document.getElementById('value').value;
        var date = document.getElementById('datepicker').value;
        var ticket_price = document.getElementById('ticket_price').value
        var v_name = validate_name(name);
        var v_owner = validate_owner(owner);
        var v_phone = validate_phone(phone);
        var v_value = validate_value(value);
        var v_date = validate_date(date);
        var v_ticket_price = validate_ticket_price(ticket_price);
        if (!v_name) {
            document.getElementById('e_name').innerHTML = "The name is invalid";
            result = false;
        } else {
            document.getElementById('e_name').innerHTML = "";
        }
        if (!v_owner) {
            document.getElementById('e_owner').innerHTML = "The owner name is invalid";
            result = false;
        } else {
            document.getElementById('e_owner').innerHTML = "";
        }
        if (!v_phone) {
            document.getElementById('e_phone').innerHTML = "The phone its incorrect";
            result = false;
        } else {
            document.getElementById('e_phone').innerHTML = "";
        }
        if (!v_value) {
            document.getElementById('e_value').innerHTML = "The value number its incorrect";
            result = false;
        } else {
            document.getElementById('e_value').innerHTML = "";
        }
        if (!v_value) {
            document.getElementById('e_date').innerHTML = "The date its incorrect";
            result = false;
        } else {
            document.getElementById('e_date').innerHTML = "";
        }
        if (!v_ticket_price) {
            document.getElementById('e_ticket_price').innerHTML = "The ticket price its incorrect";
            result = false;
        } else {
            document.getElementById('e_ticket_price').innerHTML = "";
        }
        if(result != false){
            $.ajax({
                url: "module/events/controller/controller_events.php?op=create",
                type: 'POST',
                data : {
                    "name": document.getElementById('name').value,
                    "owner": document.getElementById('owner').value,
                    "phone": document.getElementById('phone').value,
                    "value": document.getElementById('value').value,
                    "s_time": document.getElementById('s_time').value,
                    "f_time": document.getElementById('f_time').value,
                    "date": document.getElementById('datepicker').value,
                    "lng": document.getElementById('lng').value,
                    "lat": document.getElementById('lat').value,
                    "ticket_price": document.getElementById('ticket_price').value,
                    "recommended": "+16",
                },
                success: function(data) {
                    alert("Succesfull created");
                    window.location.href="index.php?page=controller_events&op=list";
                },
          })
        }else{
            document.getElementById('createbutt').style.color ='rgb( 146 , 0 ,0 , .5)';
        }
    });
});