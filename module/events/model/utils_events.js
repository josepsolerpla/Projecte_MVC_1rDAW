function validate_name(name) {
    if (name.length > 0) {
        var regexp = /^[a-zA-Z0-9]*$/;
        return regexp.test(name);
    }
    return false;
};

function validate_owner(owner) {
    if (owner.length > 0) {
        var regexp = /^[a-zA-Z0-9]*$/;
        return regexp.test(owner);
    }
    return false;
};
function validate_value(value) {
    if (value.length > 0) {
        var regexp = /^[0-9]+(\.[0-9]+)?$/;
        console.log(value+regexp.test(value));
        return regexp.test(value);
    }
    return false;
};
function validate_phone(phone) {
    if (phone.length > 0) {
        var regexp = /^[+34]+[0-9]{9}/i;
        return regexp.test(phone);
    }
    return false;
};
function validate_date(date) {
    if (date == "") {
        return true;
    }
    return false;
};
function validate_ticket_price(ticket_price) {
    if (ticket_price.length > 0) {
        var regexp = /^[0-9]+(\.[0-9]+)?$/;
        return regexp.test(ticket_price);
    }
    return false;
};

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)        {
            return decodeURIComponent(sParameterName[1]);
        }
    }
};
var loadName = function () {
        var source_names=[];
        find_byname.forEach(function(rdo) {
            source_names.push(rdo.nombre);
        });
        $("#search_name").autocomplete({
            source: source_names,
            close: function( event, ui ) {
                var name = document.getElementById('search_name').value;
                $.get("module/events/controller/controller_events.php?op=list&name="+name, function (data) {
                    find_byname = JSON.parse(data);
                    if(find_byname === 'error') {
                            window.location.href='index.php?page=503';
                        }else{
                            load_info(name);    
                        };
                });
            }
        });
    };
var load_info = function (search_name) {
    $.get("module/events/controller/controller_events.php?op=list&name="+search_name, function (data, status) {
        var json = JSON.parse(data);
        while(document.getElementById("eventdivlist_02")){
        document.getElementById("eventdivlist_02").remove();
        };
        json.forEach(function(rdo){
            var newElement = document.createElement('div');
            newElement.id = "eventdivlist_02";
            newElement.innerHTML = '<h3>'+rdo.nombre+'</h3>\<p>'+rdo.fecha+'</p>';
            newElement.innerHTML = newElement.innerHTML + '<div class="event_read" id="'+rdo.id_event+'">Info</div>';
            newElement.innerHTML = newElement.innerHTML + '<div class="event_update" id="'+rdo.id_event+'">Update</div>';
            newElement.innerHTML = newElement.innerHTML + '<div class="event_delete" id="'+rdo.id_event+'">Delete</div>';
            newElement.innerHTML = newElement.innerHTML + '<div class="event_tickets" id="'+rdo.id_event+'">Tickets</div>';
            document.getElementById("tablelist_events").appendChild(newElement);
        });
    });                
};
var load_ticket = function (id_event_ticket) {
    $.get("module/events/controller/controller_events.php?op=list_tickets&id_event="+id_event_ticket, function (data, status) {
        var json = JSON.parse(data);
        while(document.getElementById("ticketlist")){
        document.getElementById("ticketlist").remove();
        };
        json.forEach(function(rdo){
            var newElement = document.createElement('div');
            newElement.id = "ticketlist";
            newElement.className = rdo.id_event;
            newElement.innerHTML = '<h3>'+rdo.nombre+'</h3>\<p>'+rdo.code_ticket+'</p>';
            if (rdo.c_nombre != null) {
                newElement.innerHTML = newElement.innerHTML+'<p>'+rdo.c_nombre+'</p>';
            }else{
                newElement.innerHTML = newElement.innerHTML+'<p>Not assigned</p>';
            }
            document.getElementById("listtickets").appendChild(newElement);
        });
    });
};