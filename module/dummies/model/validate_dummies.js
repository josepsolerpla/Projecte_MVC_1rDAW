function cant(user) {
    if (user.length > 0) {
        var regexp = /^[1-9][0-9]?$|^100$/;
        return regexp.test(user);
    }
    return false;
}

function validate_number() {
    var result = true;

    var number = document.getElementById('dummies').value;
	var v_number = cant(number);
    
    if (!v_number) {
        document.getElementById('e_dummie').innerHTML = "The number its incorrect";
        result = false;
    } else {
        document.getElementById('e_dummie').innerHTML = "";
    }
    if(result===false){
        return false;
    }else {
        document.form_dummies.submit();
        document.form_dummies.action="index.php?page=controller_dummies&op=create";
    }
}