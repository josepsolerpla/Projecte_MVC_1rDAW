<?php 
//	CHECK IF THE SESSION IS ALLOWED TO ENTER
	if( (($_SESSION['login_usertype']) != 'admin') and (($_SESSION['login_usertype']) != 'owner') ){
		die('<script>window.location.href="index.php?page=503"</script>');
	}
//	INCLUDES
	include("module/dummies/model/DAODummies.php");
//	CONTROLLER
	switch ($_GET['op']) {
			case 'delete':
				try{
					$daodummies = new DAODummies();
					$rdo = $daodummies->delete_event_dummies();
				}catch (Exception $e){
					$callback = 'index.php?page=503';
					die('<script>windw.location.href="'.$callback .'";</script>');
				}

		 		if($rdo){
	     			$callback = 'index.php?page=controller_events&op=list';
		 		die('<script>window.location.href="'.$callback .'";</script>');
		 		}else{
	     			$callback = 'index.php?page=503';
		 	        die('<script>window.location.href="'.$callback .'";</script>');
	     		}
				break;
			case 'create':
				$error = false;
			 	if (isset($_POST['dummies'])) {
			 		$daodummies = new DAODummies();
		 			$dummies = $daodummies->create_dummies($_POST);

					if(isset($dummies)){
	        			$callback = 'index.php?page=controller_events&op=list';
	    			    die('<script>window.location.href="'.$callback .'";</script>');
	        		}else{
	        			$callback = 'index.php?page=503';
				        die('<script>window.location.href="'.$callback .'";</script>');
			 		}
			 	}
			 	include 'module/dummies/view/create_dummies.php';
				break;
			}