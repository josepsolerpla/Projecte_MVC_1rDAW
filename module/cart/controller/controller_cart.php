<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . '/Projecte_MVC_1rDAW/';
	include($path."module/events/model/find_events.php");
	include($path."module/events/model/DAOTickets.php");
	include($path."module/users/model/DAOUser.php");
	include($path."model/connect.php");
	if (!isset($_SESSION)) {
	  session_start();
	}
	switch($_GET['op']) {
		case 'find_eventbyid':
			try{
				$findevent = new FINDuser();
				$_SESSION['update_id']=$_GET['id_event'];
				$find_rdo = $findevent->find_eventbyid();
			}catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$find_rdo){
    			echo json_encode("error");
                exit;
    		}else{
               	echo json_encode($find_rdo);
                exit;
    		}
			break;
		case 'userlike':
			try{
				$givelike = new DAOUser();
				$gl = $givelike->givelike_f($_SESSION['id'],$_POST['id_event']);
			}catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$gl){
    			echo json_encode("error");
                exit;
    		}else{
               	echo json_encode("true");
                exit;
    		}
			break;
		case 'logedornot':
			if(empty($_SESSION['id'])){
				echo json_encode("nologed");
			}else
				echo json_encode("loged");
			break;
		case 'howmuchleft';
			try{
				$_SESSION['id_event_hf']=$_POST['id_event_hf'];
				$hwmuchtick = new DAOTickets();
				$cant_t = $hwmuchtick->event_howmuch_left();
			}catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$cant_t){
    			echo json_encode("false");
                exit;
    		}else{
               	echo json_encode($cant_t);
                exit;
    		}
			break;
		case 'payment':
			if(empty($_SESSION['id'])){
				echo json_encode("nologed");
			}else{
				if(!isset($_SESSION["payment_tickets"])){
					$_SESSION["payment_tickets"]=[];
				}
				$events_tick = new stdClass();
				$events_tick->id_event = $_POST['cart_idevent'];
				$events_tick->cant_idevent = $_POST['cant_idevent'];
				$events_tick->price = $_POST['price'];
				array_push($_SESSION["payment_tickets"], $events_tick);

				$_SESSION['cart_idevent'] = $_POST['cart_idevent'];
				$_SESSION['cart_cant'] = $_POST['cant_idevent'];
				try{
					$assign_ticket = new DAOTickets();
					$ticket = $assign_ticket->assign_ticket();
				}catch (Exception $e){
	                echo json_encode("error");
	                exit;
	            }
	            if(!$ticket){
	    			echo json_encode("error");
	                exit;
	    		}else{
	               	echo json_encode("true");
	                exit;
	    		}
			}
			break;
		default:
		case 'list':
			include($path."module/cart/view/cart.php");
			break;
		}