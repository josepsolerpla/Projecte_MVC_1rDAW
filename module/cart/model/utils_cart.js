var cart = [];
function limit_on_cart(id,callback){
    var cont;
    $.ajax({
            url: "module/cart/controller/controller_cart.php?op=howmuchleft",
            type: 'POST',
            dataType: "json",
            data: {
              "id_event_hf": id,
            },
            success: function(data) {
              cont = data.count;
            },
    });
    return cont;
}
function give_like(id_event){
  const promise = new Promise(function (resolve, reject) {
        $.ajax({
        url: "module/cart/controller/controller_cart.php?op=userlike",
        type: 'POST',
        dataType: "json",
        data: {
          "id_event": id_event,
        },
        success: function(data) { 
            resolve(data);
            return data;
          },
        });
      })
    return promise;
}
function searchbyid(id) {
  const promise = new Promise(function (resolve, reject) {
    $.ajax({
        url: "module/cart/controller/controller_cart.php?op=find_eventbyid&id_event="+id,
        type: 'GET',
        dataType: "json",
        success: function(data) {
          resolve(data);
          return data;
        },
      })//End of Ajax
     });//End of Promise
  return promise;
}
function calc() {
  return promise = new Promise(function (resolve, reject) {
    tot=0;
    len=cart.length;
    cart.forEach(function(cart, i) {
      searchbyid(cart.id).then(function(data) {
        if (isNaN(parseInt(cart.qty))) {cart.qty=0};
        tot = parseInt(tot)+(data[0].ticket_price*parseInt(cart.qty)) 
        if ( i == len - 1) {resolve(tot)};
      });
    })
  });
}
function recalculatetotal() {
  total=$("#total_price_ticket")[0].innerHTML;
  total= 0;
  calc().then(function(a) {
  $("#total_price_ticket")[0].innerHTML=a+"€"
  });
}
function logedornot() {
  const promise = new Promise(function (resolve,reject) {
    $.ajax({
          url: "module/cart/controller/controller_cart.php?op=logedornot",
          type: 'GET',dataType: "json",
          success: function(data) {
            resolve(data);            
            return data;
          }
      });
  })
  return promise;
}
function getqty(id) {
      for (var i in cart) {
              if(cart[i].id == id){
                  return cart[i].qty;
              }
            } 
    }
function addToCart(id) {
// update qty if product is already present
    for (var i in cart) {
        if(cart[i].id == id){
            return;
        }
    }
// create JavaScript Object
    var item = { "id": id, "qty": 1 };
    cart.push(item);
    saveStuff();
}
function changeCart(id,qty) {
  deleteItem(id);
  var item = { "id": id, "qty": qty};
  cart.push(item);
  saveStuff();
}
function saveStuff() {
  if ( window.localStorage){
      localStorage.cart = JSON.stringify(cart);
  }
}
function emptyStuff() {
  if ( window.localStorage){
      localStorage.removeItem(cart);
  }
}
function deleteItem(id){
  for (var i in cart) {
    if(cart[i].id == id){
      var index = i;
    }
  }
  cart.splice(index,1); 
  saveStuff();
}
function hmlft (rdo) {
      const promise = new Promise(function (resolve, reject) {
        $.ajax({
        url: "module/cart/controller/controller_cart.php?op=howmuchleft",
        type: 'POST',
        dataType: "json",
        data: {
          "id_event_hf": rdo.id,
        },
        success: function(data) {             
            var cant = data.count
            resolve(cant);
            return cant;
          },
        });
        if (!rdo) {
          reject(new Error('error'))
        }
      })
    return promise;
}
function savecartsession(ct) {
  const promise = new Promise(function (resolve, reject) {
    ct.forEach(function(rp) {
      if (rp.qty <= 0) {console.log("Quantity no valid")}else{
      var cant = hmlft(rp).then(function(resp){
        if(resp > 0){
            $.ajax({
              url: "module/cart/controller/controller_cart.php?op=payment",
              type: 'POST',dataType: "json",
              data: {
                  "cart_idevent": rp.id,
                  "cant_idevent": rp.qty,
                  "price": "20",
              },success: function(data) {
                resolve(data);
              }
            })//End of ajax
        }//End of if
      });
      };
      deleteItem(rp.id);
    });//End of forEach
  });//End of primise
  return promise
}