<div id="aboutus">
	<h1>You will find</h1>
	<p>Esta aplicacion se compone de 6 modulos en formato MVC</p>
  <li>Users</li>
  <li>Events</li>
  <li>Dummies</li>
  <li>Homepage</li>
  <li>Login</li>
  <li>Cart</li>
  <p>Los modulos se componen de un controlador el cual tiene puesto un sistema para que no todos los usuarios puedan entrar o dependiendo de la accion(Archivo : /model/authorize.php)
    ,</p>
  <p>El modulo de Users(Usuarios) se encuentra un controller en formato php (Archivo : /module/users/controller/controller_users.php) para recibir peticiones de javascript y devolver valores
    ,luego un archivo de javascript que pintara dinamicamente los datos ya estemos en la vista de List-Read-Update,ademas de validar las entradas de datos del usuario
    ,ademas al crear o modificar un usuario comprueba que ese nombre y telefono no este ya en uso,este modulo contiene un modal el cual se rellena desde el servidor el cual "no esta bien hecho" o como deberia hacerse.
  </p>
  
  <p>El modulo de Events(Eventos) tiene dos archivos DAO (DAOEvents y DAOTickets) ya que los eventos o mas bien los tickets es nuestro producto de venta, las vistas y funcionamiento de estas
    es similar a la de los demas, se rellena dinamicamente y valida la existencia y datos,este modulo contiene un modal diferente al de Usuarios este si se rellena dinamicamente
    dependiento de que evento se seleccione. El modulo se compone de listado de eventos,borrar eventos,crear eventos,crear dummimes,listar tickets(especificos de cada evento)
    . Cada evento tiene sus propios tickets, los tickets se crearan incrementalmente, estos tickets estaran asignados en la base de datos a el evento y a un codigo de usuario el cual sera
    null por defecto</p>
  <p>Millores : </p>
  <li>Buscador en el menu de administrar + Autocomplete</li>
  <li>Modal en JS</li>
  <li>Dummies</li>
  <li>Validacion si un nombre ya existe te lo dice al momento</li>
  <li>Sistema de tickets per a cada event</li>
  <p>El modulo/pagina de Homepage esta distribuida de la misma forma que los otros modulos, esta pagina contiene un loading screen para que el usuario vea ya la pagina
      100% cargada, un slider en la parte superior, una vista de un mapa, geolocalizando tu ubicacion, ademas incorpora un marcador en el mapa con los eventos ya que tienen
      como datos las cordenadas, debajo de el tenemos un buscador con filtros,autocomplete,desplegables y paginacion los cuales tiene un boton para acceder a su modal dinamico y añadirlo al carrito
      ,por ultimo en la parte inferior tiene un listado de eventos desde api</p>
  <p>Millores : </p>
  <li>Chorraes varies de CSS</li>
  <li>Google Maps + Geolocalitzat + Marker per cada event</li>
  <li>Buscador de Events + Autocomplete + Drop Down</li>
  <li>Paginacion</li>
  <li>Si esta Sold Out se muestra</li>
  <li>API muestra eventos que se terminan hoy su venta</li>
  <p>En toda la aplicacion podremos acceder desde el menu al apartado de login el cual tiene su propio sistema de validacion/existencia de usuario,registro,remember password. Cuando uno esta
    logeado camibara tanto lo que ve como lo que puede acceder, cada usuario tiene un tipo el cual tiene permisos en cada uno de los modulos. El usuario puede acceder a su perfil
    desde el desplegable de login</p>
 <p>Al carrito se añadiran todos aquellos productos los cuales queden existencias de entradas, el usuario podra ver en su carrito cada ticket que tiene con su cantidad y precio,
  tambien encontramos el total con el boton de pagar, para realizar las compras tendremos que estar logeados y los tickets se asignan a el id del cliente, el usuario podra ver sus 
  tickets en el perfil</p>
  <p>Millores : </p>
  <li>No se puede añadir un ticket que no tiene cantidad</li>
  <li>Contador al momento del total</li>
  <li>Validacion de Cant (Solo numero >= 0)</li>
</div>