	<div id="create_users">
    <h1>Create user's</h1> 
    <form name="formulario_usuario" id="formulario_usuario" method="post">
      <div id="formulari">
        <div id="slot">
	       <p>
	        <label for="name">User's name :</label>
	        <input name="name" class="name" type="text" id="name" placeholder="name" value="<?php @$_POST['name'] ?>" >
	        <span id="e_user" class='error_js'></span>
          <?php
          if(isset($error['error_name']))
              print_r ("<div id=result><SPAN class='error'>" . "".$error['error_name'] . "</SPAN></div>");
          ?>
	       </p>
        </div> 
        <div id="slot">
        	<p>
          	<label for="surname">User's surname :</label>
        		<input name="surname" class="surname" type="text" id="surname" placeholder="surname" value="<?php @$_POST['surname'] ?>" >
          	<span id="e_surname" class='error_js'></span>
          </p>
        </div>
        
        <div id="slot">
         	<p>
         		<label for="email">User's Email :</label>
         		<input name="email" class="email" type="text" id="email" placeholder="email" value="<?php @$_POST['email'] ?>">
         		<span id="e_email" class='error_js'></span>
            <?php
            if(isset($error['error_email']))
                    print_r ("<div id=result><SPAN class='error'>" . "".$error['error_email'] . "</SPAN></div>");
            ?>
         	</p>
        </div>
        
        <div id="slot">
        	<p>
          	<label for="phone">User's number phone :</label>
          	<input name="phone" class="phone" type="text" id="phone" placeholder="number phone" value="<?php @$_POST['phone'] ?>">
          	<span id="e_phone" class='error_js'></span>
            <?php
            if(isset($error['error_phone']))
                    print_r ("<div id=result><SPAN class='error'>" . "".$error['error_phone'] . "</SPAN></div>");
            ?>
          </p>
        </div>
        
        <div id="slot">
        	<p>
          	<label for="password1">Password :</label>
          	<input name="password1" class="password1" type="password" id="password1" placeholder="password" value="<?php @$_POST['password1'] ?>">
          	<span id="e_password1" class='error_js'></span>
         	</p>
        </div>
        <div id="slot">
        	<p>
        	  <label for="password2">Repeat PASSWORD :</label>
        	  <input name="password2" class="password2" type="password" id="password2" placeholder="password" value="<?php @$_POST['password2'] ?>">
        	  <span id="e_password2" class='error_js'></span>
          </p>
        	</p>
        </div>
        
        <div id="slot">
        	<p>
        	  <label for="birthay">Birthday :</label>
        	  <input name="birthay" class="birthay" type="text" id="datepicker" placeholder="birthday" value="<?php @$_POST['value'] ?>" readonly="readonly">
        	  <span id="e_birthday" class='error_js'></span>
        	</p>
        </div>
        <div id="button">
          <input name="done" class="done" type="button" value="Done" onclick="validate_user()"/>
        </div>
      </div>
    </form>
    </div>