<?php
echo '<div class="modal fade" id="readmodal'.$row['id'].'"tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">';	
?>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
		<h1>User Information</h1>
			<table class="table_read_users">
				<tr>
					<td class="slot">ID: </td>
					<td>
						<?php
							echo $row['id'];
						?>
					</td>
				</tr>
				<tr>
					<td class="slot">User: </td>
					<td>
						<?php
							echo $row['name'];
						?>
					</td>
				</tr>
				<tr>
					<td class="slot">Surname: </td>
					<td>
						<?php
							echo $row['surname'];
						?>
					</td>
				</tr>
				<tr>
					<td class="slot">Email: </td>
					<td>
						<?php
							echo $row['email'];
						?>
					</td>
				</tr>
				<tr>
					<td class="slot">Phone: </td>
					<td>
						<?php
							echo $row['phone'];
						?>
					</td>
				</tr>
				<tr>
					<td class="slot">Password: </td>
					<td>
						<?php
							echo $row['password'];
						?>
					</td>
				</tr>
			</table>      
		</div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>