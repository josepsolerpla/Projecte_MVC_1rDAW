<div id="list">
	<div id="listtittle">
		<?php echo '<h1>' .$i18n["List Users"] .'</h1>'; ?>
	</div>
	<div>
		<p>
			<?php echo '<a style="position: relative;top: 0;left: 45%;background: rgb(0,19,82,0.6);font-size: 28px;" class="addlist" href="index.php?page=controller_users&op=create">' .$i18n["Add User"] .'</a>'; ?>
		</p>
	</div>
	<div id="tablelist">
		<table>
			<tr>
				<?php echo '<td>' .$i18n["Name"] .'</td>'; ?>
				<?php echo '<td>' .$i18n["Phone"] .'</td>'; ?>
				<?php echo '<td>' .$i18n["Type"] .'</td>'; ?>
			</tr>
		<?php
			if($rdo->num_rows === 0){
				echo '<tr>';
				echo '<td>' .$i18n["You need to create a user"] .'</td>';
				echo '</tr>';
			}else {
				foreach ($rdo as $row) {
					echo '<tr>';
					echo '<td>'.$row['id'].'</td>';
					echo '<td>'.$row['name'].'</td>';
					echo '<td>'.$row['phone'].'</td>';
					echo '<td>'.$row['surname'].'</td>';
					echo '<td>';
					echo '<a href="#" class="btn btn-lg btn-success" data-toggle="modal" data-target="#readmodal'.$row['id'].'"\>' .$i18n["Read"] .'</a>';
					include("module/users/view/read_with_modal.php");
					echo '<a href="index.php?page=controller_users&op=update&id='.$row['id'].'">' .$i18n["Update"] .'</a>';
					echo '<a href="#" class="btn btn-lg btn-success" data-toggle="modal" data-target="#deletemodal'.$row['id'].'"\>' .$i18n["Delete"] .'</a>';
					include("module/users/view/delete_with_modal.php");
					echo '</td>';
					echo '</tr>';
				}
			}
		?>
		</table>
	</div>
</div>