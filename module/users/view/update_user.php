<div id="update_users">
  <?php print_r($user)?>
	<form autocomplete="on" method="post" name="update_user" id="update_user" onsubmit="return validate_users_php();" action="index.php?page=controller_users&op=update">
		<div>
			<h1>Modify the user</h1><input name="id" value="<?php echo $user['id'] ?>" readonly="readonly"></input>
		</div>
		<div id="slot">
	    <p>
	      <label for="name">User's name :</label>
	      <input name="name" class="name" type="text" id="name" placeholder="name" value="<?php echo $user['name'] ?>" >
	      <span id="e_user" class='error_js'></span>
        <?php
        if ($error['error_name'])
            print_r ("<div id=result><SPAN class='error'>" . "".$error['error_name'] . "</SPAN></div>");
        ?>
	    </p>
    </div>
    <div id="slot">
      <p>
        <label for="surname">User's surname :</label>
        <input name="surname" class="surname" type="text" id="surname" placeholder="surname" value="<?php echo $user['surname'] ?>" >
        <span id="e_surname" class='error_js'></span>
      </p>
    </div>
    <div id="slot">
      <p>
        <label for="email">User's Email :</label>
        <input name="email" class="email" type="text" id="email" placeholder="email" value="<?php echo $user['email'] ?>">
        <span id="e_email" class='error_js'></span>
        <?php
        if ($error['error_email'])
            print_r ("<div id=result><SPAN class='error'>" . "".$error['error_email'] . "</SPAN></div>");
        ?>
      </p>
    </div>
    <div id="slot">
      <p>
        <label for="phone">User's number phone :</label>
        <input name="phone" class="phone" type="text" id="phone" placeholder="number phone" value="<?php echo $user['phone'] ?>">
        <span id="e_phone" class='error_js'></span>
        <?php
        if ($error['error_phone'])
                print_r ("<div id=result><SPAN class='error'>" . "".$error['error_phone'] . "</SPAN></div>");
        ?>
      </p>
    </div>
    <div id="slot">
      <p>
        <label for="password1">Password :</label>
        <input name="password1" class="password1" type="password" id="password1" placeholder="password" value="<?php echo $user['password'] ?>">
        <span id="e_password1" class='error_js'></span>
      </p>
    </div>
    <div id="slot">
      <p>
        <label for="birthay">Birthday :</label>
        <input name="birthay" class="birthay" type="text" id="datepicker" placeholder="birthday" value="<?php echo $user['birthday'] ?>" readonly="readonly">
        <span id="e_birthday" class='error_js'></span>
      </p>
    </div>
    <div id="slot">
      <p>
        <label for="user_type">User Type :</label>
        <input name="user_type" class="user_type" type="text" placeholder="user_type" value="<?php echo $user['user_type'] ?>">
        <span id="e_user_type" class='error_js'></span>
      </p>
    </div>
    <div id="button">
      <input name="update" class="done" type="submit" value="Update"/>
    </div>
	</form>
</div>