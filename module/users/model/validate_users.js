function validate_name(user) {
    if (user.length > 0) {
        var regexp = /^[a-zA-Z0-9]*$/;
        return regexp.test(user);
    }
    return false;
}

function validate_surname(surname) {
    if (surname.length > 0) {
        var regexp = /^[a-zA-Z0-9]*$/;
        return regexp.test(surname);
    }
    return false;
}
function validate_email(email) {
    if (email.length > 0) {
        var regexp = /^[A-Z0-9._%+-]+@(?:[A-Z0-9-]+.)+[A-Z]{2,4}$/i;
        return regexp.test(email);
    }
    return false;
}
function validate_phone(phone) {
    if (phone.length > 0) {
        var regexp = /^[+34]+[0-9]{9}/i;
        return regexp.test(phone);
    }
    return false;
}
function validate_password1(password1) {
    if (password1.length > 0) {
        var regexp = /^[a-zA-Z0-9]*$/;
        return regexp.test(password1);
    }
    return false;
}
function validate_password2(password1, password2) {
	if(password1 === password2){
		return true;
    }
    return false;
}

function validate_user() {
    var result = true;

    var user = document.getElementById('name').value;
	var surname = document.getElementById('surname').value;
	var email = document.getElementById('email').value;
	var phone = document.getElementById('phone').value;
    var password1 = document.getElementById('password1').value;
	var password2 = document.getElementById('password2').value;
    //var birthay = document.getElementById('birthay').value;

	var v_user = validate_name(user);
	var v_surname = validate_surname(surname);
	var v_email = validate_email(email);
    var v_phone = validate_phone(phone);
	var v_password = validate_password1(password1);
	var v_password2 = validate_password2(password1, password2);

    
    if (!v_user) {
        document.getElementById('e_user').innerHTML = "The user its incorrect";
        result = false;
    } else {
        document.getElementById('e_user').innerHTML = "";
    }

	if (!v_surname) {
        document.getElementById('e_surname').innerHTML = "The surname its incorrect";
        result = false;
    } else {
        document.getElementById('e_surname').innerHTML = "";
    }

	if (!v_email) {
        document.getElementById('e_email').innerHTML = "The email its incorrect";
        result = false;
    } else {
        document.getElementById('e_email').innerHTML = "";
    }

    if (!v_phone) {
        document.getElementById('e_phone').innerHTML = "The phone number its incorrect";
        result = false;
    } else {
        document.getElementById('e_phone').innerHTML = "";
    }

	if (!v_password) {
        document.getElementById('e_password1').innerHTML = "The password its incorrect";
        result = false;
    } else {
        document.getElementById('e_password1').innerHTML = "";
    }

	if (!v_password2) {
        document.getElementById('e_password2').innerHTML = "The passwords doesn't match eachother";
        result = false;
    } else {
        document.getElementById('e_password2').innerHTML = "";
    }
    if(result===false){
        return false;
    }else {
        document.formulario_usuario.submit();
        document.formulario_usuario.action="index.php?page=controller_users";
    }


}