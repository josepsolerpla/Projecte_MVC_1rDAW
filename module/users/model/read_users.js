function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)        {
            return decodeURIComponent(sParameterName[1]);
        }
    }
};
$(document).ready(function() {
    if(document.getElementById("read_users") !== null){
        var id = GetURLParameter("id");
    $.get("module/users/controller/controller_users.php?op=read&id=" + id, function (data, status) {
        var json = JSON.parse(data);
        if(json === 'error') {
            window.location.href='index.php?page=503';
        }else{
            $("#id_user").html(json.id);
            $("#name_user").html(json.name);
            $("#surname_user").html(json.surname);
            $("#email_user").html(json.email);
            $("#password_user").html("**********");
            $("#phone_user").html(json.phone);
            $("#birthday_user").html(json.birthday);
            $("#type_user").html(json.user_type);
        }
    });
    $.ajax({
        url: "module/users/controller/controller_users.php?op=mytickets",
        type: 'POST',
        success: function(data) {
            var json = JSON.parse(data);
            if(json != 'empty'){
                console.log(json);
                json.forEach(function(rdo){
                    var newElement = document.createElement('div');
                    newElement.id = "eventdivlist_01";
                    newElement.className = rdo.code_ticket;
                    newElement.innerHTML = '<h3>'+rdo.nombre+'</h3>\<p>'+rdo.code_ticket+'</p>';
                    document.getElementById("listtickets").appendChild(newElement);
                });
            }else{
                var newElement = document.createElement('div');
                    newElement.innerHTML = '<h2>No tickets</h2>';
                    document.getElementById("listtickets").appendChild(newElement);
            }
        },
    })
    $.ajax({
        url: "module/users/controller/controller_users.php?op=mylikes",
        type: 'POST',
        success: function(data) {
            console.log(data);
            var json = JSON.parse(data);
            if(json != 'empty'){
                console.log(json);
                json.forEach(function(rdo){
                    var newElement = document.createElement('div');
                    newElement.id = "user_like";
                    newElement.className = rdo.code_ticket;
                    newElement.innerHTML = '<h3>'+rdo.name+'</h3>\<p>'+rdo.id_event+'</p>';
                    document.getElementById("listlikes").appendChild(newElement);
                });
            }
        },
    })
    $(document).on('click','#eventdivlist_01',function() {
        var doc = new jsPDF();
        var id_this = $(this)[0].className;
        console.log(id_this);
        
        doc.text(20, 20, id_this);
        doc.text(20, 30, "rdo.code_ticket");
        doc.output('dataurlnewwindow');
    });
    $("#more_info_profile").click(function() {
        $("#profile_moreinfo").fadeIn();
        $("#read_users").css('height',"500px")
    });
    }
    
});