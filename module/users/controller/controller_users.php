<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . '/Projecte_MVC_1rDAW/';
	if (!isset($_SESSION)) {
	  session_start();
	}
//	CHECK IF THE SESSION IS ALLOWED TO ENTER
//	INCLUDES
	include($path."module/events/model/DAOTickets.php");
	include($path."module/users/model/DAOUser.php");
	include($path."module/users/model/find_user.php");
    include($path."model/connect.php");
    include($path."module/users/model/validate_users.php");
//	CONTROLLER
    if(isset($_GET['op'])){
	switch ($_GET['op']) {
		case 'list':
			try{
				$daouser = new DAOUser();
				$rdo = $daouser->select_all_users();
			}catch (Exception $e){
				$callback = 'index.php?page=503';
				die('<script>windw.location.href="'.$callback .'";</script>');
			}
			if(!$rdo){
				$callback = 'index.php?page=503';
				die('<script>windw.location.href="'.$callback .'";</script>');				
			}else{
				include("module/users/view/list_user.php");
			}
			break;
		case 'create':
		 	$error = false;
		 	if (isset($_POST['phone'])) {
				$error=validate_users_php();
		 		if(!$error['error']) {
		 			$_SESSION['user']=$_POST;
		 			
		 			$user = $_SESSION['user'];

					$daouser = new DAOUser();
					$rdo = $daouser->new_user($user);
					if(isset($rdo)){
						if(($_SESSION['login_usertype'] != 'admin') and ($_SESSION['login_usertype'] != 'owner')){
							$callback = 'index.php?page=controller_home&op=list';
        			    	die('<script>window.location.href="'.$callback .'";</script>');
						}
            			$callback = 'index.php?page=controller_home&op=list';
        			    die('<script>window.location.href="'.$callback .'";</script>');
            		}else{
            			$callback = 'index.php?page=503';
    			        die('<script>window.location.href="'.$callback .'";</script>');
            		}		
		 		}
		 	}
		 	include 'module/users/view/create_user.php';
		 	break;
		case 'read':
		 	try{
                $daouser = new DAOUser();
            	$rdo = $daouser->select_user($_SESSION['id']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
    			echo json_encode("error");
                exit;
    		}else{
    		    $user=get_object_vars($rdo);
                echo json_encode($user);
                exit;
    		}
		 	break;
		 case 'mytickets':
		 	try{
                $daotickets = new DAOTickets();
                $tickets = $daotickets->mytickets($_SESSION['id']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$tickets){
    			echo json_encode("error");
                exit;
    		}else{
                echo json_encode($tickets);
                exit;
    		}
		 	break;
        case 'mylikes':
            try{
                $daolikes = new DAOUser();
                $likes = $daolikes->mylikes();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$likes){
                echo json_encode("error");
                exit;
            }else{
                echo json_encode($likes);
                exit;
            }
            break;
		 case 'delete':
		 	if(isset($_POST['delete'])){
		 		try{
		 			$daouser = new DAOUser();
		 			$rdo = $daouser->delete_user($_GET['id']);
		 		}catch (Exception $e){
		 			$callback = 'index.php?page=503';
     			    die('<script>window.location.href="'.$callback .'";</script>');
		 		}

		 		if($rdo){
         			$callback = 'index.php?page=controller_users&op=list';
		 		die('<script>window.location.href="'.$callback .'";</script>');
		 		}else{
         			$callback = 'index.php?page=503';
		 	        die('<script>window.location.href="'.$callback .'";</script>');
         		}
		 	}
		 	include("module/users/view/delete_user.php");
		 	break;
		 case 'update':
            $error = false;
			$error_phone = false;
			$error_email = false;
            if (isset($_POST['update'])){
            	$error=validate_users_php();
                if (isset($error)){
                    $_SESSION['user']=$_POST;
                    try{
                        $daouser = new DAOUser();
    		            $rdo = $daouser->update_user($_POST);
                    }catch (Exception $e){
                        $callback = 'index.php?page=503';
        			    die('<script>window.location.href="'.$callback .'";</script>');
                    }
		            if(isset($rdo)){
            			$callback = 'index.php?page=controller_users&op=list';
        			    die('<script>window.location.href="'.$callback .'";</script>');
            		}else{
            			$callback = 'index.php?page=503';
    			        die('<script>window.location.href="'.$callback .'";</script>');
            		}
                }
            }
            try{
                $daouser = new DAOUser();
            	$rdo = $daouser->select_user($_GET['id']);
            	$user=get_object_vars($rdo);
            }catch (Exception $e){
                $callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
    			$callback = 'index.php?page=503';
    			die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
        	    include("module/users/view/update_user.php");
    		}
            break;

		default:
            include("view/includes/error404.php");
			break;
		}
    	}else 
	        include("view/includes/error404.php");
