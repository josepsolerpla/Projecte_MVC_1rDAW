<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . '/Projecte_MVC_1rDAW/';
	include($path."module/events/model/DAOEvents.php");
	include($path."module/homepage/model/DAOHome.php");
	include($path."module/events/model/find_events.php");
	include($path."module/events/model/DAOTickets.php");

	switch ($_GET['op']) {
		case 'list':
			include("module/homepage/view/list_homepage.php");
			break;
		case 'buttons':
			if(isset($_GET)){
				$_SESSION['num']=$_GET['num'];
				$_SESSION['search_name'] = $_GET['name'];
				$_SESSION['search_lat'] = $_GET['lat'];
				$_SESSION['search_date'] = $_GET['date'];	
			}else {
				$_SESSION['search_name'] = "";
				$_SESSION['search_lat'] = "";
				$_SESSION['search_date'] = "";					
			}
			try{
				if($_GET['action']=="next"){		
					$daoevent = new DAOHomepage();
					$rdo_action = $daoevent->select_events_next();
				}else if($_GET['action']=="back"){
					$daoevent = new DAOHomepage();
					$rdo_action = $daoevent->select_events_back();
	            }else if($_GET['action']==""){
					$daoevent = new DAOHomepage();
					$rdo_action = $daoevent->select_events_default();
				}
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }	
            if(!$rdo_action){
    			echo json_encode("error");
                exit;
    		}else{
                echo json_encode($rdo_action);
                exit;
    		}
			break;
		case 'find_byname':
			try{
				$findevent = new FINDuser();
				$find_rdo = $findevent->find_event_home01($_GET['name']);
			}catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$find_rdo){
    			echo json_encode("error");
                exit;
    		}else{
               	echo json_encode($find_rdo);
                exit;
    		}
			break;
		case 'find_bylat':
			try{
				$findevent = new FINDuser();
				$find_rdo = $findevent->find_event_home02($_GET['name'],$_GET['lat']);
			}catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$find_rdo){
    			echo json_encode("error");
                exit;
    		}else{
               	echo json_encode($find_rdo);             
    		}
			break;
		case 'find_close':
			try{
				$findevent = new FINDuser();
				$find_rdo = $findevent->find_event_home();
			}catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$find_rdo){
    			echo json_encode("error");
                exit;
    		}else{
            	echo json_encode($find_rdo);
                exit;
    		}
			break;
    	 case 'count_events':
    	 	if(isset($_GET)){
				$_SESSION['num']=$_GET['num'];
				$_SESSION['search_name'] = $_GET['name'];
				$_SESSION['search_lat'] = $_GET['lat'];
				$_SESSION['search_date'] = $_GET['date'];	
			}else {
				$_SESSION['search_name'] = "";
				$_SESSION['search_lat'] = "";
				$_SESSION['search_date'] = "";					
			}
        	try{
                $daoevent = new DAOHomepage();
            	$rdo = $daoevent->count_events();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
    			echo json_encode("error");
                exit;
    		}else{
                echo json_encode($rdo);
                exit;
    		}
            break;
        case 'howmuchleft';
			try{
				$_SESSION['id_event_hf']=$_POST['id_event_hf'];
				$hwmuchtick = new DAOTickets();
				$cant_t = $hwmuchtick->event_howmuch_left();
			}catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$cant_t){
    			echo json_encode("false");
                exit;
    		}else{
               	echo json_encode($cant_t);
                exit;
    		}
			break;
		}