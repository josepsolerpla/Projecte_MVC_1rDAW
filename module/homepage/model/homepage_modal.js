$(document).on('click','.home_read',function(){
    $('.home_read').click(function () {
            var id = this.getAttribute('id');
            //alert(id);
            
            $.get("module/events/controller/controller_events.php?op=read_modal&modal=" + id, function (data, status) {

                var json = JSON.parse(data);
                //console.log(json);
                
                if(json === 'error') {
                    //console.log(json);
                    //pintar 503;
                    window.location.href='index.php?page=503';
                }else{
                    //console.log(json.user);
                    $("#nombre").html(json.nombre);
                    $("#propietario").html(json.propietario);
                    $("#telf_contact").html(json.telf_contact);
                    $("#dirigido_a").html(json.dirigido_a);
                    $("#presupuesto").html(json.presupuesto);
                    $("#hora_inicio").html(json.hora_inicio);
                    $("#hora_fin").html(json.hora_fin);
                    $("#fecha").html(json.fecha);
         
                    $("#details_user").show();
                    $("#user_modal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 500, //<!--  ------------- altura de la ventana -->
                        //show: "scale", //<!-- ----------- animación de la ventana al aparecer -->
                        //hide: "scale", //<!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        //position: "down",<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            //effect: "blind",
                            duration: 0
                        },
                        hide: {
                            //effect: "explode",
                            duration: 0
                        }
                    })
                }
            });
        });
});