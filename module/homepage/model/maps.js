function initMap() {
//Marcadors		lng lat
        $.get("module/homepage/controller/controller_home.php?op=find_close", function (data, status) {
            markers_location = JSON.parse(data);
            markers_location.forEach(function(op){
              var contentString = '<h1>'+op.nombre+'</h1>'+
                                  '<p>'+op.hora_inicio+'</p>'+
                                  '<p>'+op.fecha+'</p>'+
                                  '<p>'+op.dirigido_a+'</p>';
              var marker = new google.maps.Marker({
                position: {lat: Number(op.lat),lng: Number(op.lng)},
                map: map,
                title: op.nombre,
                animation: google.maps.Animation.DROP,
              });
              var infowindow = new google.maps.InfoWindow({
                content: contentString
              });
              marker.addListener('click', function() {
                infowindow.open(map, marker);
              });
            });
        });
        var centre_iesestacio = {lat: 38.810145, lng: -0.604237};
//Centrar Mapa y Crear
        var map = new google.maps.Map(document.getElementById('map_home'), {
          zoom: 10,
          center: centre_iesestacio
        });
// Try HTML5 geolocation.
        var infoWindow = new google.maps.InfoWindow({map: map});
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('You are here.');
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
//Markers
      }
      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
      }