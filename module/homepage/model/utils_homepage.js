   function counttickets(rdo) {
      const promise = new Promise(function (resolve, reject) {
            $.ajax({
            url: "module/homepage/controller/controller_home.php?op=howmuchleft",
            type: 'POST',
            dataType: "json",
            data: {
              "id_event_hf": rdo.id_event,
            },
            success: function(data) {             
                var cant = data.count
                resolve(cant);
                return cant;
              },
            });
            if (!rdo) {
              reject(new Error('error'))
            }
          })
        return promise;
    }
    var loadName = function () {
        var source_names=[];
        find_byname.forEach(function(rdo) {
            source_names.push(rdo.nombre);
        });
        $("#input").autocomplete({
            source: source_names,
        });
    };
    var loadLat = function () {
        var source_lat=[];
        find_bylat.forEach(function(rdo) {
            source_lat.push(rdo.lat);
        });
        $("#search_lat").autocomplete({
            source: source_lat,
            close: function( event, ui ) {
                home_name = document.getElementById('input').value;
                home_lat = document.getElementById('search_lat').value;
                $.get("module/homepage/controller/controller_home.php?op=find_bylat&name="+home_name+"&lat="+home_lat, function (data) {
                    find_bydate = JSON.parse(data);
                    if(find_bydate === 'error') {
                            window.location.href='index.php?page=503';
                        }else{
                         loadDate();
                        };
                    });
                if(document.getElementById('search_lat').value == ''){
                    $("#search_date").empty();
                }
            }
        });
    };
    var loadDate = function () {
        $("#search_date").empty();
        $("#search_date").append("<option value=''>ALL</option>");
        $.each(find_bydate, function (i, valor) {
            $("#search_date").append("<option value='" + valor.fecha + "'>" + valor.fecha + "</option>");
        });
    };
    var load_info = function (action,num,search_name,search_lat,search_date) {
        $.get("module/homepage/controller/controller_home.php?op=buttons&action="+action+"&num="+num+"&name="+search_name+"&lat="+search_lat+"&date="+search_date, function (data, status) {
            var json = JSON.parse(data);
            if(json === 'error') {
                window.location.href='index.php?page=503';
            }else{
                var count;
                $.get("module/homepage/controller/controller_home.php?op=count_events&action="+action+"&num="+num+"&name="+search_name+"&lat="+search_lat+"&date="+search_date, function (data, status) {
                   var a =JSON.parse(data);
                   count = a[0].count;
                    while(document.getElementById("eventdivlist")){
                    document.getElementById("eventdivlist").remove();
                    };
                    json.forEach(function(rdo){
                        var newElement = document.createElement('div');
                        newElement.id = "eventdivlist";
                        newElement.innerHTML = '<h3>'+rdo.nombre+'</h3>\<p>'+rdo.fecha+'</p>';
                        newElement.innerHTML = newElement.innerHTML + '<div class="home_read" id="'+rdo.id_event+'">Info</div>';
                        newElement.innerHTML = newElement.innerHTML + '<div class="home_like" id="'+rdo.id_event+'">Like</div>';
                        counttickets(rdo).then(function(resp) {
                            if (resp > 0) {
                                newElement.innerHTML = newElement.innerHTML + '<div class="cart_add" id="'+rdo.id_event+'">Add cart</div>';
                            }else {
                                newElement.innerHTML = newElement.innerHTML + '<div class="cart_add_01" ">SOLD OUT</div>';
                            }
                        });
                        document.getElementById("tablelist_homepage").appendChild(newElement);
                    });
                   if((parseInt(count)) >= (parseInt(num+9))){
                        enter_b="yes";
                        enter_n="no";
                    }else{
                        enter_b="no";
                        enter_n="yes"; 
                    }
                    if ((parseInt(count)) <= (parseInt(num+9))) {
                        enter_b="yes";
                        enter_n="yes";
                    }else{
                        enter_n="no";                        
                    }
                });                
            };
        });
    };
//API SALES OUT TODAY
    var api = function () {
        //https://developer.ticketmaster.com/api-explorer/
        var fullDate = new Date();
        var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
        var twoDigitsDay = ("0" + fullDate.getDate()).slice(-2);
        var currentDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + twoDigitsDay;
      $.ajax({
        url: "https://app.ticketmaster.com/discovery/v2/events.json?apikey=a3qjeN7cq4tipfldblfMw0mFGx0aBawZ&onsaleEndDateTime="+currentDate+"T23:59:00Z&city=Spain",
        dataType: "json",
        success: function(json) {
            while(document.getElementById("apieventlist")){
               document.getElementById("apieventlist").remove();
            };
            json._embedded.events.forEach(function(rdo){
                var newElement = document.createElement('div');
                newElement.id = "apieventlist";
                newElement.className= rdo.id;
                newElement.innerHTML = '<h2>'+rdo.name+'</h2>\<a href="'+rdo.url+'">Website</a>';
                document.getElementById("apidiv").appendChild(newElement);
                $("."+rdo.id).css("background-image", "url("+rdo.images[1].url+")");
            });
           },
      })
    };