var find_byname;
var action="";
var find_bylat;
var find_bydate;
var home_lat;
var home_name = "";
var num = 0;
var enter_b = "yes";
var enter_n = "no";
var search = "";
$(document).ready(function () {
    api();
// CHARGE THE FIRST PAGE OF DISPLAY
    $.get("module/homepage/controller/controller_home.php?op=buttons&action=&num=0&name=&lat=&date=", function (data, status) {
            var json = JSON.parse(data);
            num=0;
            if(json === 'error') {
                window.location.href='index.php?page=503';
            }else{
                json.forEach(function(rdo){
                    var newElement = document.createElement('div');
                    newElement.id = "eventdivlist";
                    newElement.innerHTML = '<h3>'+rdo.nombre+'</h3>\<p>'+rdo.fecha+'</p>';
                    newElement.innerHTML = newElement.innerHTML + '<div class="home_read" id="'+rdo.id_event+'">Info</div>';
                    newElement.innerHTML = newElement.innerHTML + '<div class="home_like" id="'+rdo.id_event+'">Like</div>';
                    counttickets(rdo).then(function(resp) {
                        if (resp > 0) {
                            newElement.innerHTML = newElement.innerHTML + '<div class="cart_add" id="'+rdo.id_event+'">Add cart</div>';
                        }else {
                            newElement.innerHTML = newElement.innerHTML + '<div class="cart_add_01" ">SOLD OUT</div>';
                        }
                    });
                    document.getElementById("tablelist_homepage").appendChild(newElement);
                });
            };
        });
//    SEARCH BUTTON
    $('#search_home').click(function() {
        var search_name = document.getElementById("input").value;
        var search_lat = document.getElementById("search_lat").value;
        var search_date = document.getElementById("search_date").value;
        num=0;
        load_info(action,num,search_name,search_lat,search_date);
    });
// SEARCH ON INPUT KEYP UP
    $("#input").click(function(){
        $("#arrow1").fadeOut();
    });
    $('#input').on('input', function() {
        var search_name = document.getElementById("input").value;
        var search_lat = document.getElementById("search_lat").value;
        var search_date = document.getElementById("search_date").value;
        num=0;
        load_info(action,num,search_name,search_lat,search_date);
    });
    $('#search_lat').on('input', function() {
        var search_name = document.getElementById("input").value;
        var search_lat = document.getElementById("search_lat").value;
        var search_date = document.getElementById("search_date").value;
        num=0;
        load_info(action,num,search_name,search_lat,search_date);
    });
    $('#search_date').on('input', function() {
        var search_name = document.getElementById("input").value;
        var search_lat = document.getElementById("search_lat").value;
        var search_date = document.getElementById("search_date").value;
        num=0;
        load_info(action,num,search_name,search_lat,search_date);
    });
    
//    DROPDOWN + AUTOCOMPLET
    $.get("module/homepage/controller/controller_home.php?op=find_byname&name="+home_name, function (data) {
        find_byname = JSON.parse(data);
        if(find_byname === 'error') {
                window.location.href='index.php?page=503';
            }else{
                loadName();    
            };
    });
    $('#input').keyup(function() {
        home_name = document.getElementById('input').value;
        $.get("module/homepage/controller/controller_home.php?op=find_byname&name="+home_name, function (data) {
            find_bylat = JSON.parse(data);
            if(find_bylat === 'error') {
                    window.location.href='index.php?page=503';
                }else{
                    loadLat();
                };
            });
    });
    $('#search_lat').keyup(function() {
        home_name = document.getElementById('input').value;
        home_lat = document.getElementById('search_lat').value;
        $.get("module/homepage/controller/controller_home.php?op=find_bylat&name="+home_name+"&lat="+home_lat, function (data) {
            find_bydate = JSON.parse(data);
            if(find_bydate === 'error') {
                    window.location.href='index.php?page=503';
                }else{
                 loadDate();
                };
            });
    });
//    NEXT AND BACK BUTTONS
    $('.home_buttons').click(function () {
        action = this.getAttribute('id');
        var search_name = document.getElementById("input").value;
        var search_lat = document.getElementById("search_lat").value;
        var search_date = document.getElementById("search_date").value;
        switch(action){
        case "next":
            if(enter_n == "no"){
                num+=9;
            }
            break;
        case "back":
            if(enter_b == "yes"){
                num-=9;
            }
            break;
        }
        
        if(num < 0){
            num = 0;
        }
        load_info(action,num,search_name,search_lat,search_date);
    });
    //DropDown click
    var show_search = "true";
    $("#input").click(function () {
        if(show_search == "true"){
            $('.dropdivhome').toggle('slide', {
             duration: 500,
             easing: 'easeOutBounce',
             direction: 'up'
            });
            show_search="false"; // ! IMPORTANT
        };
        $("#list_homepage").css('min-height','45em');
    });
    var iScrollPos = 0;
    $(window).scroll(function () {
        var iCurScrollPos = $(this).scrollTop();
        if (iCurScrollPos > iScrollPos) {
            $("#arrow2").fadeOut();
        } else {
           //Scrolling Up
        }
        iScrollPos = iCurScrollPos;
    });
    $("#arrow2").click(function() {
        $('html, body').animate({
            scrollTop: $(".titleapi").offset().top
        }, 1000);
    });
});
$(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");;
    });