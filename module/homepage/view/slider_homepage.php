<h1 id="slider_title" style="transition: all 1s ease;color: rgb(198, 122, 197); position: absolute; z-index: 3; top: 200px; left: 50%; font-size: 90px; font-family: helvetica;"><?php echo $i18n["Welcome"];?></h1>
<div class="main_slider">
  <div class="slider-outer">
    <div class="slider">
      <div class="slide-item"><span class="slide-image" style="background-image: url(view/image/valpa5.jpg);"></span></div>
      <div class="slide-item"><span class="slide-image" style="background-image: url(view/image/valpa6.jpg);"></span></div>
      <div class="slide-item"><span class="slide-image" style="background-image: url(view/image/valpa4.jpg);"></span></div>
    </div>
  </div>
</div>
<script type="text/javascript">
  var slideCount = document.querySelectorAll('.slider .slide-item').length;
  var slideWidth = document.querySelectorAll('.slider-outer')[0].offsetWidth;
  var slideHeight = document.querySelectorAll(".slider-outer")[0].offsetHeight;

  var sliderUlWidth = slideCount * slideWidth;
  document.querySelectorAll('.slider')[0].style.cssText = "width:" + sliderUlWidth + "px";

  for (var i = 0; i < slideCount; i++) {
    document.querySelectorAll('.slide-item')[i].style.cssText = "width:" + slideWidth + "px;height:" + slideHeight + "px";
  }

  setInterval(function() {
    moveRight();
  }, 3000);
  var counter = 1;
  function getRandomColor() {
	  var letters = '0123456789ABCDEF';
	  var color = '#';
	  for (var i = 0; i < 6; i++) {
	    color += letters[Math.floor(Math.random() * 16)];
	  }
	  return color;
	}



	function setRandomColor() {
	  $("#slider_title").css("color", getRandomColor());
	}
  function moveRight() {
  	setRandomColor();
    var slideNum = counter++
      if (slideNum < slideCount) {
        var transformSize = slideWidth * slideNum;
        document.querySelectorAll('.slider')[0].style.cssText = 
          "width:" + sliderUlWidth + "px; -webkit-transition:all 800ms ease; -webkit-transform:translate3d(-" + transformSize + "px, 0px, 0px);-moz-transition:all 800ms ease; -moz-transform:translate3d(-" + transformSize + "px, 0px, 0px);-o-transition:all 800ms ease; -o-transform:translate3d(-" + transformSize + "px, 0px, 0px);transition:all 800ms ease; transform:translate3d(-" + transformSize + "px, 0px, 0px)";

      } else {
        counter = 1;
        document.querySelectorAll('.slider')[0].style.cssText = "width:" + sliderUlWidth + "px;-webkit-transition:all 800ms ease; -webkit-transform:translate3d(0px, 0px, 0px);-moz-transition:all 800ms ease; -moz-transform:translate3d(0px, 0px, 0px);-o-transition:all 800ms ease; -o-transform:translate3d(0px, 0px, 0px);transition:all 800ms ease; transform:translate3d(0px, 0px, 0px)";
      }

  }
</script>