<?php
	if (isset($_GET['page'])){
		switch ($_GET['page']) {
		case 'controller_users':
			include("view/includes/top_pages/top_page_users.php");
			break;
		case 'controller_events':
			if (!isset($_GET["op"])) {
				$_GET["op"]="list";
			}
			switch ($_GET['op']) {
				default:
				case 'list':
					include("view/includes/top_pages/top_page_events_list.php");
					break;
				case 'update':
					include("view/includes/top_pages/top_page_events_update.php");
					break;
				case 'create':
					include("view/includes/top_pages/top_page_events_create.php");
					break;
				case 'tickets':
					include("view/includes/top_pages/top_page_events_tickets.php");
					break;
			}
			break;
		case 'controller_home':
			include("view/includes/top_pages/top_page_home.php");
			break;
		case 'controller_dummies':
			include("view/includes/top_pages/top_page_dummies.php");
			break;
		case 'cart':
			include("view/includes/top_pages/top_page_cart.php");
			break;
		default:
			include("view/includes/top_pages/top_page.php");
			break;
		}
	}else
		include("view/includes/top_pages/top_page.php");

	header("Access-Control-Allow-Origin: https://app.ticketmaster.com/discovery/v2/events.json?");
	header("Access-Control-Allow-Credentials: true");
	
	if (!isset($_SESSION)) {
	  session_start();
	}
	if (!isset($_SESSION['login_usertype'])) {
		$_SESSION['login_usertype']="";
	}
	if(!isset($_SESSION['username'])){
		$_SESSION['username']="";
	}	
	include("model/lenguages.php");
?>
<div id="fullpage">
	<div id="header">
		<?php include("view/includes/header.php"); ?>
	</div>
	<div>
		<?php //include("module/homepage/view/slider_homepage.php");?>
	</div>
	<div id="menu">
		<?php include("view/includes/menu.php"); ?>
	</div>
		<div id="content">
			<?php include("view/includes/pages.php");?>
		</div>

	<div id="footer">
		<?php include("view/includes/footer.php");?>
	</div>
</div>
<?php include("view/includes/bottom_page.php"); ?>