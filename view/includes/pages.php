<?php
	include("model/authorize.php");
	if(!isset($_GET['page']))
		$_GET['page'] = '';
	switch ($_GET['page']) {
		case 'controller_home':
			include("module/homepage/controller/".$_GET['page'].".php");
			break;
		case 'results_users':
			include("module/users/view/result_users/".$_GET['page'].".php");
			break;
		case 'aboutus':
			include("module/aboutus/".$_GET['page'].".php");
			break;
		case 'controller_events':
			author_op('read_modal');
			switch ($_GET['op']) {
				case 'list':
					include("module/events/view/list_events.php");
					break;
				case 'update':
					include("module/events/view/update_events.php");
					break;
				case 'create':
					include("module/events/view/create_events.php");
					break;
				case 'tickets':
					include("module/events/view/create_tickets.php");
					break;
			}
			break;
		case 'controller_users':
		author_op2('read','create');
			switch ($_GET['op']) {
				case 'read':
					include("module/users/view/read_user.php");
					break;
				
				default:
					include("module/users/controller/".$_GET['page'].".php");
					break;
			}
			break;
		case '404';
			include("view/includes/error".$_GET['page'].".php");
			break;
		case '503';
			include("view/includes/error".$_GET['page'].".php");
			break;
		case 'controller_dummies':
			include("module/dummies/controller/".$_GET['page'].".php");
			break;
		case 'cart':
			if(!isset($_GET['op'])){
				$_GET['op'] = "list";
			}
			if($_GET['op'] == "pay_conf"){
				include("module/cart/controller/controller_cart.php");
			}else {
				include("module/cart/controller/controller_cart.php");
			}
			break;
		default:
			$_GET['op']="list";
			include("module/homepage/controller/controller_home.php");
			break;
	}
?>